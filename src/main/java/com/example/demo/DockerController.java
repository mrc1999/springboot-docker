package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Copyright (C), 2015-2019
 * FileName: DockerController
 * Author:   MRC
 * Date:     2019/10/21 21:37
 * Description: 测试
 * History:
 */
@RestController
@RequestMapping
public class DockerController {

    @GetMapping("/")
    public String index() {
        return "Hello Docker-spring";
    }

}